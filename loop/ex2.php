<!-- Exercice 2 Créer deux variables. Initialiser la première à 0 et la deuxième avec un nombre compris en 1 et 100.
Tant que la première variable n'est pas supérieur à 20 : 
    - multiplier la première variable avec la deuxième
    - afficher le résultat
    - incrementer la première variable
-->

<?php

$coffee = 0;
$water = 50;

while ($coffee <= 20) {
    $result= $coffee * $water;
    echo "<p>$result</p>";  
    $coffee ++;
}
?>